#include "export_ag.h"
int32_t get_e_enum_e_t_type(int32_t e_enum_name) {
    if (e_enum_name >= START_EXPORT_SETT_ENUM && e_enum_name <= END_EXPORT_SETT_ENUM) {
        return E_G_COMM_TYPE_SETTING;
    }

    if (e_enum_name >= START_EXPORT_EVENT_ENUM && e_enum_name <= END_EXPORT_EVENT_ENUM) {
        return E_G_COMM_TYPE_EVENT;
    }

    if (e_enum_name >= START_EXPORT_RES_ENUM && e_enum_name <= END_EXPORT_RES_ENUM) {
        return E_G_COMM_TYPE_RESULT;
    }

    return END_EXPORT_COMM_TYPE_ENUM;
}
